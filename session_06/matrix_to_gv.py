# coding:utf-8

# User-Centred Data Visualization (UCDV) Examples
# Copyright (C) 2018 Enrico Costanza, University College London

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import sys

# get the input filename as the first argument we get from the terminal
in_fname = sys.argv[1]

# the output filename is the same as the input filename, except
# that we replace the extension from '.csv' to '.gv' 
# (replace) is a method of the string object in python
out_fname = in_fname.replace('.csv', '.gv')

# we open the input file..
in_file = open(in_fname, 'r', encoding='utf-8')
# ..and create a csv reader around it
reader = csv.reader(in_file)

# we open a file to write our output
# i.e. the description of the graph in 
# graphviz format (i.e. in the 'dot language)
out_file = open(out_fname, 'w', encoding='utf-8')

# the first thing we need to write is this:
initial_sting = "graph {"
# we write it into the file and add a newline 
out_file.write(initial_sting + "\n")

# the function next returns the next row in the csv reader 
# as we just started, this is the first line, i.e. the header row
heading = next(reader)

# create a set to store the edges of the graph
edges = set()

# iterate over the rows in the csv file
for row in reader:
    # the first item of the row is the name
    name1 = row[0]
    # iterate over the row items 
    # the function enumerate iterates and provide the index of the current item
    # the second argument specifies what value the index should start from
    for index, value in enumerate(row[1:], 1):
        # we are now on colun index, so we can get the name 
        # corresponding to this column from the heading
        name2 = heading[index]

        # skip "collaborations with oneself"
        if name1 == name2:
            continue

        # the csv reader treats all data as text
        # so we need to convert the values to int
        # if we want to treat them as numbers
        value = int(value)
        if value > 0:
            # the edge can be define as the pair of names 
            current_edge = (name1, name2)
            # sort the pair of names, so we avoid duplicates
            current_edge = sorted(current_edge)
            # the sorted function returns a list, we need to convert 
            # the list to tuple, otherwise we cannot use this for the set
            # (this is because lists are mutable, while sets only like 
            # immutable objects, tuples are immutable)
            current_edge = tuple(current_edge)
            # if the current edge is not already in the set..
            if current_edge not in edges:
                # ..we add it (so we do not duplicate things later)
                edges.add(current_edge)

                # and now we create a string to represent the edge 
                # in the dot language
                # a basic version could look like this:
                #edge_string = '"%s" -- "%s";" % (name1, name2)
                # in alternative, we may want to represent the number of shared papers
                # as the thickness of the edge (that's 'penwidth' in graphviz)
                edge_string = '"%s" -- "%s" [penwidth=%d];' % (name1, name2, value)
                # print for sanity check
                print(edge_string)
                # write the edge description to the output file
                # and add a new line after it
                out_file.write(edge_string + "\n")

# after we iterate over all rows, we need to "close" the graph
final_string = "}"
# we write the final string and add a new line
out_file.write(final_string + "\n")
