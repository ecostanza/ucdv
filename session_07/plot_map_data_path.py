# coding: utf-8

# User-Centred Data Visualization (UCDV) Examples
# Copyright (C) 2018 Enrico Costanza, University College London

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import pandas as pd
import geopandas as gpd
from matplotlib import pyplot as plt

# this is a bit of a hack to get the geopandas library to read the KML file
# based on https://gis.stackexchange.com/a/258370
import fiona
fiona.drvsupport.supported_drivers['LIBKML'] = 'rw' # enable KML support which is disabled by default

# use the first command line argument as the map data filename
map_fname = sys.argv[1]
wards = gpd.read_file(map_fname)
# sanity check
# print(wards.head())

# the second command line argument is the name of the file with the 
# statistical data about the wards
data_fname = sys.argv[2]
# we read this using the standard pandas method
# note the encoding of this file should be latin1 
# (you may need to change this if you open this 
# file in some editor that changes its encoding)
df = pd.read_csv(data_fname, header=0, encoding='latin1')
# sanity check
# print(df.head())

# use the third command line argument as the path filename
path_fname = sys.argv[3]
path = gpd.read_file(path_fname)

# change the path to have the same coordinate reference system (crs) 
# as the wards (based on http://geopandas.org/projections.html 
# and in particular http://geopandas.org/projections.html#re-projecting )
path = path.to_crs(wards.crs)

# let's merge the wards geodataframe and the 
# df dataframe base on the 'code' columns:
# (for more information about pd.merge see https://pandas.pydata.org/pandas-docs/stable/merging.html )
merged = pd.merge(wards, df, left_on='GSS_CODE', right_on='New Code', how='inner')
# print(merged.head())

# let's create a matplotlib figure, so that we can retain the handle to the axis
fig, ax = plt.subplots()

# the column names of the data are very long, 
# so for convenience let's store the name of
# the column we want to plot in a variable:
selected = 'Life Expectancy; Life expectancy at birth (all persons); 2009-2013'

# let's create a choropleth map
# the legend=true argument tells plot to show the legend (based on https://stackoverflow.com/questions/31755886/choropleth-map-from-geopandas-geodatafame )
merged.plot(column=selected, cmap='Greens', legend=True, ax=ax)

# plot the path, in blue 
path.plot(color='blue', ax=ax)

# let's use the column name as a title for the chart
# however, to make it more legible, let's replace ';' by line-breaks
label = selected.replace(';',"\n")
# let's also add to the title "(merged by code)"
plt.title(label)

# constrain zoom (based on https://stackoverflow.com/questions/2934878/matplotlib-pyplot-preserve-aspect-ratio-of-the-plot)
plt.axis('equal')

plt.show()


