# coding: utf-8

# User-Centred Data Visualization (UCDV) Examples
# Copyright (C) 2018 Enrico Costanza, University College London

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import pandas as pd
import geopandas as gpd
from matplotlib import pyplot as plt

# this is a bit of a hack to get the geopandas library to read the KML file
# based on https://gis.stackexchange.com/a/258370
import fiona
fiona.drvsupport.supported_drivers['LIBKML'] = 'rw' # enable KML support which is disabled by default

# use the first command line argument as the map data filename
map_fname = sys.argv[1]
wards = gpd.read_file(map_fname)
# sanity check
# print(wards.head())

# use the second command line argument as the path filename
path_fname = sys.argv[2]
path = gpd.read_file(path_fname)

# change the path to have the same coordinate reference system (crs) 
# as the wards (based on http://geopandas.org/projections.html 
# and in particular http://geopandas.org/projections.html#re-projecting )
path = path.to_crs(wards.crs)

# let's create a matplotlib figure, so that we can retain the handle to the axis
fig, ax = plt.subplots()

# plot the ward boundaries 
wards.plot(ax=ax)

# plot the path, in red 
path.plot(color='red', ax=ax)

# constrain zoom (based on https://stackoverflow.com/questions/2934878/matplotlib-pyplot-preserve-aspect-ratio-of-the-plot)
plt.axis('equal')

plt.show()

