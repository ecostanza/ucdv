# coding: utf-8

# User-Centred Data Visualization (UCDV) Examples
# Copyright (C) 2018 Enrico Costanza, University College London

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import geopandas as gpd
from matplotlib import pyplot as plt

# use the first commandline argument as the map filename
map_fname = sys.argv[1]
wards = gpd.read_file(map_fname)

# sanity check
print(wards.head())

# plot the data
wards.plot()

# constrain zoom to maintain map aspect ratio (based on 
# https://stackoverflow.com/questions/2934878/matplotlib-pyplot-preserve-aspect-ratio-of-the-plot)
plt.axis('equal')

plt.show()

