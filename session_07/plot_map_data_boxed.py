# coding: utf-8

# User-Centred Data Visualization (UCDV) Examples
# Copyright (C) 2018 Enrico Costanza, University College London

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import pandas as pd
import geopandas as gpd
from matplotlib import pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

# use the first commandline argument as the map filename
map_fname = sys.argv[1]
wards = gpd.read_file(map_fname)

# sanity check
# print(wards.head())

# the second command line argument is the name of the file with the 
# statistical data about the wards
data_fname = sys.argv[2]
# we read this using the standard pandas method
# note the encoding of this file should be latin1 
# (you may need to change this if you open this 
# file in some editor that changes its encoding)
df = pd.read_csv(data_fname, header=0, encoding='latin1')
# sanity check
print(df.head())

# let's merge the wards geodataframe and the 
# df dataframe base on the 'code' columns:
# (for more information about pd.merge see https://pandas.pydata.org/pandas-docs/stable/merging.html )
merged = pd.merge(wards, df, left_on='GSS_CODE', right_on='New Code', how='inner')

# the column names of the data are very long, 
# so for convenience let's store the name of
# the column we want to plot in a variable:
selected = 'Indices of Deprivation; Rank of income scale (within London); 2010'

# let's create a matplotlib figure, so that we can retain the handle to the axis
fig, ax = plt.subplots()

# plot the ward boundaries, this time simply 
# in white with black edges (i.e. outline)
merged.plot(color='white', edgecolor='black', ax=ax)

# add a column to the dataframe and store in it the centroid of each ward
# the centroid is provided by geopandas, and this is where we will display 
# the boxe bar corresponding to each ward
merged['centroid'] = merged['geometry'].centroid

# create an empty list to store boxes
boxes = []
# an empty list for the bars
bars = []
# w and h are the dimensions of the boxes
w = 200
h = 400.0

# iterate over the rows of the geodataframe
# however, select only the two columns we need from the dataframe
for row in merged[['centroid', selected]].itertuples():
    # row is a list-like object, the first item is 
    # the index of the row (which we do not need)
    # the second item is the centroid
    # we need to convert it to a tuple as follows 
    # to be able to use it for plotting
    pt = (row[1].x, row[1].y)
    # the third item of the row is the value of the 'selecte' column
    value = row[2]

    # let's create a Rectangle object positione at pt
    # and of size w, h
    # see http://matthiaseisen.com/pp/patterns/p0203/
    # and https://matplotlib.org/gallery/statistics/errorbars_and_boxes.html#sphx-glr-gallery-statistics-errorbars-and-boxes-py
    curr_box = Rectangle(pt, w, h)
    # append it to the list of boxes
    boxes.append(curr_box)

    # create the bar, this is similar to the box, but the height 
    # is based on the data value
    curr_bar = Rectangle(pt, w, value*h/merged[selected].max())
    # append it to the list of bars
    bars.append(curr_bar)

# Create patch collection from the list of boxes this is so that 
# we can apply the same visual style to all the boxes, in particular 
# we colour them white, with red outline and 0.8 transparent (alpha)
boxex_pc = PatchCollection(boxes, facecolor='white', alpha=.8, edgecolor='red')

# same for bars, but this time we colour them red, and no outline
bars_pc = PatchCollection(bars, facecolor='red', alpha=.8, edgecolor=None)

# we add collection to axes, which means that we "plot" them
ax.add_collection(boxex_pc)
ax.add_collection(bars_pc)

# let's use the column name as a title for the chart
# however, to make it more legible, let's replace ';' by line-breaks
label = selected.replace(';',"\n")
plt.title(label)

# constrain zoom (based on https://stackoverflow.com/questions/2934878/matplotlib-pyplot-preserve-aspect-ratio-of-the-plot)
plt.axis('equal')
plt.show()

